#!/bin/bash

# Just put all the client nodes into the below list.

nodes=(apt168 apt152 apt129 apt183 apt159 apt161 apt142 apt138 apt167 apt184 apt166 apt179 apt149 apt133 apt156 apt162 apt126 apt130)
username=anilmr
id=0

for i in `seq 2 13`;
do
  ssh -oStrictHostKeyChecking=no ${username}@node${i}.newherd.cs6480phantomnet-PG0.utah.cloudlab.us "cd project/HERD && mkdir client-tput"
  ssh -oStrictHostKeyChecking=no ${username}@node${i}.newherd.cs6480phantomnet-PG0.utah.cloudlab.us "cd project/HERD && sudo bash local-kill.sh"
  ssh -oStrictHostKeyChecking=no ${username}@node${i}.newherd.cs6480phantomnet-PG0.utah.cloudlab.us "cd project/HERD && sudo bash run-machine.sh $id" &
  sleep 1
  id=`expr $id + 1`
  echo $id
done
