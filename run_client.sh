#!/bin/bash

# Just put all the client nodes into the below list.

username=anilmr

id=0

for i in `seq 2 13`;
do
	echo $id
  ssh -oStrictHostKeyChecking=no ${username}@node${i}.newherd.cs6480phantomnet-PG0.utah.cloudlab.us "cd /users/anilmr/project/HERD && rm -rf client-tput && mkdir client-tput"
	ssh -oStrictHostKeyChecking=no ${username}@node${i}.newherd.cs6480phantomnet-PG0.utah.cloudlab.us "cd /users/anilmr/project/HERD && sudo bash local-kill.sh"
	ssh -oStrictHostKeyChecking=no ${username}@node${i}.newherd.cs6480phantomnet-PG0.utah.cloudlab.us "cd /users/anilmr/project/HERD && sudo bash run-machine.sh $id" &
	id=`expr $id + 1`
  sleep 2
done
