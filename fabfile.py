from fabric.api import run, env, put

env.hosts = ["anilmr@node1.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node2.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node3.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node4.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node5.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node6.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node7.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node8.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node9.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node10.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node11.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node12.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
             "anilmr@node13.newherd.cs6480phantomnet-PG0.utah.cloudlab.us",
            ]

def kill_exp():
    run("cd /users/anilmr/project/HERD && sudo bash local-kill.sh")

def delete():
    run("sudo rm -rf project")

def restart():
    run("sudo init 6")

def ofed_download():
    run("wget http://www.mellanox.com/downloads/ofed/MLNX_OFED-3.0-2.0.1/MLNX_OFED_LINUX-3.0-2.0.1-ubuntu14.04-aarch64.tgz")

def install_hugepages():
    run('sudo apt-get -y install libhugetlbfs-dev')
    run("sudo sysctl -w vm.nr_hugepages=4096")
    run("cat /proc/meminfo |grep HugePages")

def host_info():
    run('cat /etc/issue && uname -a')

def update_apt():
    run("sudo apt-get -y update && sudo apt-get -y upgrade")

def install_rdma():
    run('sudo apt-get -y install libibverbs1 ibverbs-utils librdmacm1 rdmacm-utils libmlx4-1 librdmacm-dev libibverbs-dev')

def install_java():
    run("sudo apt-get -y install default-jdk")

def install_ofed():
    run('tar -xzf MLNX_OFED_LINUX-3.0-2.0.1-ubuntu14.04-aarch64.tgz')
    run('cd MLNX_OFED_LINUX-3.0-2.0.1-ubuntu14.04-aarch64 && sudo ./mlnxofedinstall --force')

def copy_file():
    put('load.sh','/users/anilmr')

def check_rdmacm_modules():
    run('modinfo rdma_cm | head -1')

def load_modules():
    run('sudo sh load.sh')

def rename():
    run('sudo mv project_arm project')

def check_modules():
    run('lsmod | grep rdma_ucm')

def check_ibvdevices():
    run('ibv_devices')

def install_git():
    run('sudo apt-get install git')

def copy_project():
    put('/Users/anil/Research/project_arm','/users/anilmr')

def install_project():
    run('cd project/HERD && make clean && make')

def copy_server_file():
    put('/Users/anil/Research/project_arm/servers','/users/anilmr/project/HERD')

def configure_hugepages():
    run('sudo sysctl -w vm.nr_hugepages=4096')

def init_shm():
    run("sudo sysctl -w kernel.shmmax=2147483648 && sudo sysctl -w kernel.shmall=2147483648 && sudo sysctl -p /etc/sysctl.conf")

def rping():
    run("rping -c -a 10.10.1.1 -C1")

def configure_zipf():
    run("cd project/HERD/YCSB/src/ && sudo rm -rf /dev/zipf && sudo mkdir /dev/zipf && sudo java zipf")
